<?php

namespace Infinity\CoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    const INFINITY_SOFTWARE_CDN = 'CDNBundle';
    const INFINITY_SOFTWARE_CRM = 'CRMBundle';
    const INFINITY_SOFTWARE_CMS = 'CMSBundle';

    /** @var string */
    private $alias;

    public function __construct($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return array
     */
    public function getExistingSoftwares()
    {
        return array(
            'CDN'   => self::INFINITY_SOFTWARE_CDN,
            'CMS'   => self::INFINITY_SOFTWARE_CMS,
            'CRM'   => self::INFINITY_SOFTWARE_CRM,
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root($this->alias);

        $rootNode
            ->children()
                ->arrayNode('softwares')
                    ->cannotBeOverwritten()
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->prototype('scalar')
                    ->validate()
                        ->ifNotInArray(array_keys($this->getExistingSoftwares()))
                        ->thenInvalid('%s is not a valid Infinity software.')
                    ->end()
                ->end()
        ;

        return $treeBuilder;
    }
}
