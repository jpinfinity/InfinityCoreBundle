<?php

namespace Infinity\CoreBundle;

use Infinity\CoreBundle\DependencyInjection\InfinityCoreExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfinityCoreBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new InfinityCoreExtension();
        }

        return $this->extension;
    }
}
