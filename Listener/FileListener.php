<?php

namespace Infinity\CoreBundle\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Infinity\CoreBundle\Model\FileInterface;

class FileListener implements EventSubscriber
{
    public function __construct()
    {
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var FileInterface $file */
        $file = $args->getEntity();
        if($file instanceof FileInterface){
            $file->setCreatedAt(new \DateTime());
            $this->preSave($file);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        /** @var FileInterface $file */
        $file = $args->getEntity();
        if($file instanceof FileInterface){
            $file->setUpdatedAt(new \DateTime());
            $this->preSave($file);
        }
    }

    /**
     * @param FileInterface $file
     */
    private function preSave(FileInterface &$file)
    {
        $file->setSize($file->getFile()->getSize());
        $file->setInitialFilename($file->getFile()->getClientOriginalName());
    }
}
