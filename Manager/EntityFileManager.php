<?php

namespace Infinity\CoreBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class EntityFileManager implements EntityFileManagerInterface
{
    /** @var string */
    protected $allowedMimeTypeKey;

    /** @var string */
    protected $excludedMimeTypeKey;

    /** @var array */
    protected $allowedMimeTypes;

    /** @var array */
    protected $excludedMimeTypes;

    /**
     * EntityFileManager constructor.
     *
     * @param array $excludedMimeTypes
     * @param array $allowedMimeTypes
     */
    public function __construct(array $excludedMimeTypes, array $allowedMimeTypes)
    {
        $this->excludedMimeTypes = $excludedMimeTypes;
        $this->allowedMimeTypes = $allowedMimeTypes;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        if($this->getFile() instanceof UploadedFile && false === $this->getFile()->isValid()){
            return false;
        }

        return $this->isAllowedMimeType($this->getFile()->getMimeType())
            || false === $this->isExcludedMimeType($this->getFile()->getMimeType());
    }

    /**
     * @param $mimeType
     *
     * @return bool
     */
    private function isExcludedMimeType($mimeType)
    {
        list($type, $mime) = explode('/', $mimeType);
        $excludedMimeTypes = $this->excludedMimeTypes[$this->excludedMimeTypeKey];

        return (is_array($excludedMimeTypes)
                && !empty($excludedMimeTypes[$type])
                && ('*' === $excludedMimeTypes[$type] || in_array($mime, $excludedMimeTypes[$type]))
        );
    }

    /**
     * @param $mimeType
     *
     * @return bool
     */
    private function isAllowedMimeType($mimeType)
    {
        list($type, $mime) = explode('/', $mimeType);
        $allowedMimeTypes = $this->allowedMimeTypes[$this->allowedMimeTypeKey];

        return (is_null($allowedMimeTypes)
            || (is_array($allowedMimeTypes)
                && !empty($allowedMimeTypes[$type])
                && ('*' === $allowedMimeTypes[$type] || in_array($mime, $allowedMimeTypes[$type])))
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    abstract public function getFile();

    /**
     * @return string
     */
    abstract public function getDirectory();

    /**
     * @return mixed
     */
    abstract public function upload();
}
