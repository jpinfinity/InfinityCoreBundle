<?php

namespace Infinity\CoreBundle\Manager;

interface EntityFileManagerInterface
{
    /**
     * EntityFileManager constructor.
     *
     * @param array $excludedMimeTypes
     * @param array $allowedMimeTypes
     */
    public function __construct(array $excludedMimeTypes, array $allowedMimeTypes);

    public function getFile();

    public function getDirectory();

    public function isValid();

    public function upload();
}
