<?php

namespace Infinity\CoreBundle\Model;

use Symfony\Component\HttpFoundation\File\File;

interface FileInterface
{
    /**
     * @param string $filename
     */
    public function setFilename($filename);

    /**
     * @param string $initialFilename
     */
    public function setInitialFilename($initialFilename);

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt);

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt);

    /**
     * @param File $file
     */
    public function setFile(File $file);

    /**
     * @param float $size
     */
    public function setSize($size);

    /**
     * @param string $mimeType
     */
    public function setMimeType($mimeType);

    /**
     * @return File
     */
    public function getFile();
}
