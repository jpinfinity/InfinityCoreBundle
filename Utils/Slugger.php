<?php

namespace Infinity\CoreBundle\Utils;

class Slugger
{
    /** @var string */
    private $pattern = '/[^a-z0-9]/';

    /** @var string */
    private $separator = '-';

    /**
     * @param string $string
     * @return string
     */
    public function slugify($string)
    {
        return preg_replace(
            $this->pattern,
            $this->separator,
            strtolower(trim(strip_tags($string)))
        );
    }
}
